﻿/*
 * ┌─────────────────────────────────────────────────┐
 * │■■     ■      ■     ■ ■    ■■■■                                    │
 * │■■     ■      ■     ■ ■       ■                                    │
 * │■■     ■■     ■               ■                                    │
 * │■■     ■■     ■■   ■■        ■■  ■■                            │
 * │■■     ■■     ■■ ■■          ■■  ■■                            │
 * │■■■■   ■■      ■■■         ■■■■■■■■       ■■■■■■■│
 * │■■■    ■■      ■         ■■ ■■   ■■■■ ■■■■              │
 * │       ■■     ■■         ■  ■    ■                              │
 * │       ■      ■         ■■  ■    ■                              │
 * │      ■■     ■■         ■■ ■    ■■                              │
 * │     ■■       ■■    ■■   ■■     ■                                │
 * │  ■■■■         ■■■■■            ■                                │
 * └─────────────────────────────────────────────────┘
 * 
 */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

public class Player_Move : MonoBehaviour
{
    // プレイヤーの動きの値
    // 前進速度
    float forwardSpeed = 7.0f;
    // 後退速度
    float backwardSpeed = 2.0f;
    // 右速度
    float rightSpeed = 7.0f;
    // 左速度
    float leftSpeed = 7.0f;
    // 視点移動速度
    float rotateSpeed = 5.0f;
    // ジャンプ力
    float jumpPower = 3.0f;
    // 動く時のパラメータ
    Vector3 velocity;
    Vector3 velocity2;
	// Rigidbodyデータ
	Rigidbody rb;

	// Start is called before the first frame update
	void Start()
    {
		// Rigidbodyの取得
		rb = GetComponent<Rigidbody>();
		// カーソルを非表示
		Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
		var keyboard = Keyboard.current;
		var gamepad = Gamepad.current;

		float h1 = 0;
		float h2 = 0;
		float v1 = 0;
		float v2 = 0;

		bool jumpFlag = false;

		// 重力をつける
		rb.useGravity = true;

		if (gamepad != null)
		{
			h1 = gamepad.rightStick.ReadValue().x;// コントローラーの水平軸をhで定義
			h2 = gamepad.rightStick.ReadValue().y;
			v1 = gamepad.leftStick.ReadValue().y;// コントローラーの垂直軸をvで定義
			v2 = gamepad.leftStick.ReadValue().x;

			if (gamepad.bButton.isPressed)
			{
				jumpFlag = true;
			}
		}

		if (keyboard.wKey.IsPressed()) // 前に移動
		{
			v1 = 1;
		}
		if (keyboard.sKey.IsPressed()) // 後ろに移動
		{
			v1 = -1;
		}
		if (keyboard.dKey.IsPressed()) // 右に移動
		{
			v2 = 1;
		}
		if (keyboard.aKey.IsPressed()) // 左に移動
		{
			v2 = -1;
		}

		if (keyboard.rightArrowKey.isPressed) // 右を向く
		{
			h1 = 1;
		}
		if (keyboard.leftArrowKey.isPressed) // 左を向く
		{
			h1 = -1;
		}
		if (keyboard.upArrowKey.isPressed) // 上を向く
		{
			h2 = 1;
		}
		if (keyboard.downArrowKey.isPressed) // 下を向く
		{
			h2 = -1;
		}

		if (keyboard.spaceKey.isPressed)
		{
			jumpFlag = true;
		}

		// 以下、キャラクターの移動処理
		velocity = new Vector3(0, 0, v1);       // 上下のキー入力からZ軸方向の移動量を取得
		velocity2 = new Vector3(v2, 0, 0);      // 左右のキー入力からX軸方向の移動量を取得
												// キャラクターのローカル空間での方向に変換
		velocity = transform.TransformDirection(velocity);
		velocity2 = transform.TransformDirection(velocity2);
		//以下のvの閾値は、Mecanim側のトランジションと一緒に調整する
		if (v1 > 0.1)
		{
			velocity *= forwardSpeed;       // 移動速度を掛ける
		}
		else if (v1 < -0.1)
		{
			velocity *= backwardSpeed;  // 移動速度を掛ける
		}

		if (v2 > 0.1)
		{
			velocity2 *= rightSpeed;        // 移動速度を掛ける
		}
		else if (v2 < -0.1)
		{
			velocity2 *= leftSpeed; // 移動速度を掛ける
		}

		// スペースキーを入力したら
		if (jumpFlag)
		{
			//ステート遷移中でなかったらジャンプできる
			if (!anim.IsInTransition(0))
			{
				rb.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
			}
		}
	}
}
