﻿
/*
 * ┌─────────────────────────────────────────────────┐
 * │■■     ■      ■     ■ ■    ■■■■                                    │
 * │■■     ■      ■     ■ ■       ■                                    │
 * │■■     ■■     ■               ■                                    │
 * │■■     ■■     ■■   ■■        ■■  ■■                            │
 * │■■     ■■     ■■ ■■          ■■  ■■                            │
 * │■■■■   ■■      ■■■         ■■■■■■■■       ■■■■■■■│
 * │■■■    ■■      ■         ■■ ■■   ■■■■ ■■■■              │
 * │       ■■     ■■         ■  ■    ■                              │
 * │       ■      ■         ■■  ■    ■                              │
 * │      ■■     ■■         ■■ ■    ■■                              │
 * │     ■■       ■■    ■■   ■■     ■                                │
 * │  ■■■■         ■■■■■            ■                                │
 * └─────────────────────────────────────────────────┘
 * 
 * カメラは正面、後ろ、前の順番で切り替わる
 * 初期設定ではコントローラーのXボタンか、キーボードのTABキー  
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

public class Change_Camera : MonoBehaviour
{
    // カメラ切り替えフラグ
    bool camerachangeFlag;

    // 3視点のカメラ
    [SerializeField]
    GameObject MainCamera;
    [SerializeField]
    GameObject FrontCamera;
    [SerializeField]    
    GameObject BackCamera;

    void Start()
    {
        // 初期化でカメラ切り替えフラグを折る
        camerachangeFlag = false;

        // メインカメラだけをアクティブにする
        MainCamera.SetActive(true);
        FrontCamera.SetActive(false);
        BackCamera.SetActive(false);
    }

    void Update()
    {
        //キーボードとコントローラーの入力&更新
        var keyboard = Keyboard.current;
        var gamepad = Gamepad.current;

        // コントローラーのXボタンかキーボードのTABキーが押されていたらカメラ切り替えのフラグを立てる
        if((gamepad != null && gamepad.xButton.wasPressedThisFrame) || keyboard.tabKey.wasPressedThisFrame)
        {
            //押されていたらカメラ切り替えフラグを立てる
            camerachangeFlag = true;
        }
    }

    private void FixedUpdate()
    {
        // カメラ切り替えフラグが立っているとき
        if(camerachangeFlag)
        {
            if(MainCamera.activeSelf) // メインカメラがアクティブの時にバックカメラをアクティブにする
            {
                MainCamera.SetActive(false);
                FrontCamera.SetActive(false);
                BackCamera.SetActive(true);
            }
            else if(FrontCamera.activeSelf) // 前カメラがアクティブの時にメインカメラをアクティブにする
            {
                MainCamera.SetActive(true);
                FrontCamera.SetActive(false);
                BackCamera.SetActive(false);
            }
            else if(BackCamera.activeSelf) // 後ろカメラがアクティブの時に、前カメラをアクティブにする
            {
                MainCamera.SetActive(false);
                FrontCamera.SetActive(true);
                BackCamera.SetActive(false);
            }

            // 切り替え終わったらカメラ切り替えフラグを折る
            camerachangeFlag = false;
        }
    }

}
